/*
  E1
  Alexandre Santos da Silva Jr - 193093
  Marcelo Dutra Specht - 230090
*/
%{

#include "parser.h" //arquivo automaticamente gerado pelo bison
#include "cc_dict.h"

void dict_insert(char * key);

extern comp_dict_t *dict;

int line = 1;

%}

LETTER		[A-Za-z]
DIGIT		[0-9]
SPECIAL		[,;:()\[\]{}+\-/<=>!#$%&*^]

%x COMMENT

%%

int		return TK_PR_INT;
float		return TK_PR_FLOAT;
bool		return TK_PR_BOOL;
char		return TK_PR_CHAR;
string		return TK_PR_STRING;
if		return TK_PR_IF;
then 		return TK_PR_THEN;
else		return TK_PR_ELSE;
while		return TK_PR_WHILE;
do		return TK_PR_DO;
input		return TK_PR_INPUT;
output		return TK_PR_OUTPUT;
return		return TK_PR_RETURN;
const		return TK_PR_CONST;
static		return TK_PR_STATIC;
foreach		return TK_PR_FOREACH;
for		return TK_PR_FOR;
switch		return TK_PR_SWITCH;
case		return TK_PR_CASE;
break		return TK_PR_BREAK;
continue	return TK_PR_CONTINUE;
class 		return TK_PR_CLASS;
private		return TK_PR_PRIVATE;
public		return TK_PR_PUBLIC;
protected	return TK_PR_PROTECTED;

"<="		return TK_OC_LE;
">="		return TK_OC_GE;
"=="		return TK_OC_EQ;
"!="		return TK_OC_NE;
"&&"		return TK_OC_AND;
"||"		return TK_OC_OR;
">>"		return TK_OC_SR;
"<<"		return TK_OC_SL;

false		return TK_LIT_FALSE;
true		return TK_LIT_TRUE;

{SPECIAL}	return yytext[0];

[+-]?{DIGIT}+			{dict_insert(yytext);	return TK_LIT_INT;}
[+-]?{DIGIT}+\.{DIGIT}+		{dict_insert(yytext);	return TK_LIT_FLOAT;}
"'"."'"				{
					char *s = strdup(yytext+1);
					s[strlen(s)-1] = '\0';
					dict_insert(s);
					free(s);
					return TK_LIT_CHAR;
				}
"\""[^\"\n]*"\""		{
					char *s = strdup(yytext+1);
					s[strlen(s)-1] = '\0';
					dict_insert(s);
					free(s);	
					return TK_LIT_STRING;
				}
[A-Za-z_][A-Za-z0-9_]*		{dict_insert(yytext);	return TK_IDENTIFICADOR;}

\n				++line;

"/*"				BEGIN(COMMENT);
<COMMENT>"*/"			BEGIN(INITIAL);
<COMMENT>\n			{++line;}
<COMMENT>.
"//".*
[ \t]
 
.		return TOKEN_ERRO;

%%

void dict_insert(char * key)
{
	int *nLines = malloc(sizeof(int));
	*nLines = line;
	void* aux = dict_remove(dict, key);
	free(aux);
	dict_put(dict, key, nLines);
}
