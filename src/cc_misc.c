#include "cc_misc.h"
#include "cc_dict.h"
#include "main.h"

extern int line;

int comp_get_line_number (void)
{
	return line;
}

void yyerror (char const *mensagem)
{
	fprintf (stderr, "%s in line: %d\n", mensagem, line);
}

void main_init (int argc, char **argv)
{
	dict = dict_new();
}

void main_finalize (void)
{
	//dict_free(dict);
}

void comp_print_table (void)
{	
	int i;
	for (i = 0; i < dict->size; i++)
	{
		if (dict->data[i])
		{
			int *nLine = (int*)dict->data[i]->value;
			cc_dict_etapa_1_print_entrada(dict->data[i]->key, *nLine);
		}
	}
}
